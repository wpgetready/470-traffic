﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static int score;
    public GameObject text_;
    public static GameObject text;
    private static bool gameOver = false;
    public GameObject restartButton_;
    public static GameObject restartButton;
    public static bool started = false;


    // Use this for initialization
    void Start () {
        text = text_;
        restartButton = restartButton_;
        score = 0;
        gameOver = false;
        started = false;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public static void addScore()
    {
        if (gameOver) return;
        score++;
        if (text != null)
        {
            text.GetComponent<TextMesh>().text = ""+(score * 100);
        }
    }

    public static void lost()
    {
        gameOver = true;
        restartButton.SetActive(true);
    }
}
