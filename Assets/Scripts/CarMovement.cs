﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarMovement : MonoBehaviour
{

    public float thrust;
    private float timer;
    public float waitTime;
    public float maximumThrust;
    public Rigidbody rb;
    public GameObject collisionSystem;
    public bool stopped;
    public GameObject stopsign;
    public GameObject clickSound;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        timer = waitTime;
        if (stopped)
        {
            stopsign.SetActive(true);
            timer = 0;
        }
    }

    void Update()
    {
        if (timer >= waitTime) {
            if (Mathf.Abs(rb.velocity.z+ rb.velocity.x) < maximumThrust)
            {
                if(stopped)
                {
                    stopped = false;
                    stopsign.SetActive(false);
                }
                rb.AddForce(transform.forward * thrust);
            }
        } else
        {
            timer += Time.deltaTime;
        }
        if (Mathf.Abs(transform.position.x)>60||Mathf.Abs(transform.position.z)>60)
        {
            GameManager.addScore();
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        GameManager.lost();
        if (Vector3.Magnitude(rb.velocity) > 3)
        {
            GameObject p = Instantiate(collisionSystem, Vector3.Lerp(transform.position, collision.gameObject.transform.position, 0.5f), Quaternion.identity);
            Destroy(p, 5);
        }

    }

    void OnMouseDown()
    {
        Destroy(Instantiate(clickSound), 1);
        if (timer < waitTime)
        {
            timer = waitTime;
        }
        else
        {
            stopped = true;
            stopsign.SetActive(true);
            rb.velocity = Vector3.zero;
            timer = 0;
        }
    }
}
