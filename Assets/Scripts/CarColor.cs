﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarColor : MonoBehaviour
{
    void Start()
    {
        Color[] colors = {new Color(0.882f,0.302f,0.263f),
        new Color(0.639f,0.792f,0.38f),
        new Color(1f,0.435f,0.435f),
        new Color(0.984f,0.812f,0.38f),
        new Color(0.051f,0.259f,0.38f),
        new Color(0f,0.8f,0.6f),
        new Color(0f,0.396f,0.596f),
        new Color(0.51f,0.706f,0.251f),
        new Color(0.259f,0.545f,0.792f),
        new Color(0.322f,0.243f,0.486f),
        new Color(0.969f,0.494f,0.02f),
        new Color(0.141f,0.659f,0.157f),
        new Color(0.949f,0.725f,0.122f),
        new Color(0.58f,0.725f,0.596f),
        new Color(0.235f,0.741f,0.6f)
        };
        Renderer rend = GetComponent<Renderer>();
        rend.material.color = colors[Random.Range(0, colors.Length)];
    }
}
