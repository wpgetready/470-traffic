﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSpawner : MonoBehaviour {

    public GameObject prefab;
    public float duration;
    public float min;
    public float possibilityRange;
    public float timer;
    
	void Start () {
    }
    
    void Update () {
        if (GameManager.started)
        {
            if (timer < 0)
            {
                Instantiate(prefab, transform.position, transform.rotation);
                resetTimer();
            }
            timer -= Time.deltaTime;
        }
    }

    void resetTimer()
    {
        timer = min+ Mathf.Sqrt(duration*duration/(GameManager.score+1)) + Mathf.Sqrt(Random.Range(0, possibilityRange)*Random.Range(0, possibilityRange) / (GameManager.score + 1));
    }
}
