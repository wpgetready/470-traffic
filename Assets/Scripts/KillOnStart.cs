﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillOnStart : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (GameManager.started) { Destroy(gameObject); }
	}
}
